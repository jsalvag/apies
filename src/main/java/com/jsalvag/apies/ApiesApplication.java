package com.jsalvag.apies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiesApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiesApplication.class, args);
    }

}
